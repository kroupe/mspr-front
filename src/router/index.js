import Vue from 'vue'
import VueRouter from 'vue-router'
import HelloWorld from "../components/client";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'helloword',
            component: HelloWorld
        }
    ]
})

export default router
